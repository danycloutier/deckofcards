package com.dcloutier.games.deckofcards.service;

import com.dcloutier.games.deckofcards.model.Deck;
import com.dcloutier.games.deckofcards.model.PlayingCard;
import com.dcloutier.games.deckofcards.service.DeckOfCardsService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
public class DeckOfCardsServiceTests {

  private DeckOfCardsService service;

  @Before
  public void setUp() {
    service = new DeckOfCardsService();
  }

  @Test
  public void createDeckTest()
    throws Exception {

    Deck deck = service.createDeck();
    assertNotNull(deck);
    assertNotNull(deck.getId());
    assertEquals(52, deck.getRemaining());
    assertEquals(false, deck.getShuffled());
  }

  @Test
  public void retreiveTest()
    throws Exception {

    Deck deck = service.createDeck();
    deck = service.retreiveDeck(deck.getId());
    assertNotNull(deck.getId());
    assertEquals(52, deck.getRemaining());
    assertEquals(false, deck.getShuffled());
  }

  @Test(expected = IllegalArgumentException.class)
  public void retreiveNullParamTest()
    throws Exception {
    Deck deck = service.retreiveDeck(null);
  }

  @Test
  public void retreiveNonExistentDeckTest()
    throws Exception {
    Deck deck = service.retreiveDeck("1");
    assertNull(deck);
  }

  @Test
  public void shuffleTest()
    throws Exception {

    Deck deck = service.createDeck();
    service.shuffle(deck.getId());
    assertEquals(52, deck.getRemaining());
    assertEquals(true, deck.getShuffled());
  }

  @Test
  public void shufflePartialDeckTest()
    throws Exception {

    Deck deck = service.createDeck();
    deck.dealOneCard();
    service.shuffle(deck.getId());
    assertEquals(51, deck.getRemaining());
    assertEquals(true, deck.getShuffled());
  }

  @Test(expected = IllegalArgumentException.class)
  public void shuffleNullParamTest()
    throws Exception {

    service.shuffle(null);
  }

  @Test(expected = IllegalArgumentException.class)
  public void shuffleNonExistentDeckTest()
    throws Exception {

    service.shuffle("1");
  }

  @Test
  public void dealOneCardTest()
    throws Exception {

    Deck deck = service.createDeck();
    PlayingCard card = service.dealCard(deck.getId());
    assertNotNull(card);
    assertNotNull(card.getFace());
    assertNotNull(card.getSuit());
  }

  @Test(expected = IllegalArgumentException.class)
  public void dealOneCardNullParamTest()
    throws Exception {

    service.dealCard(null);
  }

  @Test(expected = IllegalArgumentException.class)
  public void dealOneCardNonExistentDeckTest()
    throws Exception {

    service.dealCard("1");
  }

}
