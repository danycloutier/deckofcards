package com.dcloutier.games.deckofcards.model;

import com.dcloutier.games.deckofcards.model.Deck;
import com.dcloutier.games.deckofcards.model.PlayingCard;
import com.dcloutier.games.deckofcards.model.PlayingCardDeck;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import static junit.framework.TestCase.assertNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
public class PlayingCardDeckTests {

  @Test
  public void createDeckTest()
    throws Exception {

    Deck deck = new PlayingCardDeck();
    assertNotNull(deck);
    assertNotNull(deck.getId());
    assertEquals(52, deck.getRemaining());
    assertEquals(false, deck.getShuffled());
  }

  @Test
  public void shuffleTest()
    throws Exception {

    Deck deck = new PlayingCardDeck();
    deck.shuffle();
    assertNotNull(deck);
    assertNotNull(deck.getId());
    assertEquals(52, deck.getRemaining());
    assertEquals(true, deck.getShuffled());
  }

  @Test
  public void shufflePartialDeckTest()
    throws Exception {

    Deck deck = new PlayingCardDeck();
    deck.dealOneCard();
    deck.shuffle();
    assertNotNull(deck);
    assertNotNull(deck.getId());
    assertEquals(51, deck.getRemaining());
    assertEquals(true, deck.getShuffled());
  }

  @Test
  public void dealOneCardTest()
    throws Exception {

    Deck deck = new PlayingCardDeck();
    PlayingCard card = deck.dealOneCard();
    assertNotNull(deck);
    assertNotNull(deck.getId());
    assertEquals(51, deck.getRemaining());
    assertEquals(false, deck.getShuffled());
    assertNotNull(card);
    assertNotNull(card.getFace());
    assertNotNull(card.getSuit());
  }

  @Test
  public void dealOneCardEmptyDeckTest()
    throws Exception {

    Deck deck = new PlayingCardDeck();

    for (int i = 0; i < 52; i++) {
      deck.dealOneCard();
    }

    PlayingCard card = deck.dealOneCard();

    assertNotNull(deck);
    assertNotNull(deck.getId());
    assertEquals(0, deck.getRemaining());
    assertEquals(false, deck.getShuffled());
    assertNull(card);
  }


}
