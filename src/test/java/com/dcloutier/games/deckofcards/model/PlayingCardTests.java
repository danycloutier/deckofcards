package com.dcloutier.games.deckofcards.model;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
public class PlayingCardTests {

  @Test
  public void createDeckTest()
    throws Exception {

    PlayingCard card = new PlayingCard(Faces.ACE, Suits.CLUBS);
    assertNotNull(card);
    assertEquals(card.getFace(), Faces.ACE);
    assertEquals(card.getSuit(), Suits.CLUBS);
  }

  @Test(expected = IllegalArgumentException.class)
  public void createDeckNullSuitTest()
    throws Exception {

    PlayingCard card = new PlayingCard(Faces.ACE, null);
  }

  @Test(expected = IllegalArgumentException.class)
  public void createDeckNullFaceTest()
    throws Exception {

    PlayingCard card = new PlayingCard(null, Suits.CLUBS);
  }

  @Test(expected = IllegalArgumentException.class)
  public void createDeckNullFaceAndSuitTest()
    throws Exception {

    PlayingCard card = new PlayingCard(null, null);
  }
}
