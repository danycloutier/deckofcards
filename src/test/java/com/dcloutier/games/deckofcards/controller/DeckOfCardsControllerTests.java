package com.dcloutier.games.deckofcards.controller;


import com.dcloutier.games.deckofcards.controller.DeckOfCardsController;
import com.dcloutier.games.deckofcards.model.Deck;
import com.dcloutier.games.deckofcards.model.PlayingCardDeck;
import com.dcloutier.games.deckofcards.service.DeckOfCardsService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.BDDMockito.doThrow;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(DeckOfCardsController.class)
public class DeckOfCardsControllerTests {

  @Autowired
  private MockMvc mvc;

  @MockBean
  private DeckOfCardsService service;

  @Test
  public void createDeckTest()
    throws Exception {

    Deck deck = new PlayingCardDeck();
    given(service.createDeck()).willReturn(deck);

    mvc.perform(post("/decks")
      .contentType(MediaType.APPLICATION_JSON))
      .andExpect(status().isCreated());
  }

  @Test
  public void retreiveDeckTest()
    throws Exception {

    Deck deck = new PlayingCardDeck();

    given(service.retreiveDeck(deck.getId())).willReturn(deck);

    mvc.perform(get("/decks/" + deck.getId())
      .contentType(MediaType.APPLICATION_JSON))
      .andExpect(status().isOk())
      .andExpect(jsonPath("$.remaining", is(52)))
      .andExpect(jsonPath("$.shuffled", is(false)));
  }

  @Test
  public void retreiveDeckNonExistentTest()
    throws Exception {

    given(service.retreiveDeck("1")).willReturn(null);

    mvc.perform(get("/decks/1")
      .contentType(MediaType.APPLICATION_JSON))
      .andExpect(status().isNotFound());
  }

  @Test
  public void shuffleDeckTest()
    throws Exception {

    Deck deck = new PlayingCardDeck();

    given(service.retreiveDeck(deck.getId())).willReturn(deck);

    mvc.perform(post("/decks/" + deck.getId() + "/shuffled")
      .contentType(MediaType.APPLICATION_JSON))
      .andExpect(status().isNoContent());
  }

  @Test
  public void shuffleNonExistentDeckTest()
    throws Exception {

    Deck deck = new PlayingCardDeck();

    doThrow(new IllegalArgumentException("Invalid DeckId")).when(service).shuffle("1");

    mvc.perform(post("/decks/1/shuffled")
      .contentType(MediaType.APPLICATION_JSON))
      .andExpect(status().isNotFound());
  }

  @Test
  public void dealOneCardTest()
    throws Exception {

    Deck deck = new PlayingCardDeck();

    given(service.dealCard(deck.getId())).willReturn(deck.dealOneCard());

    mvc.perform(get("/decks/" + deck.getId() + "/card")
      .contentType(MediaType.APPLICATION_JSON))
      .andExpect(status().isOk())
      .andExpect(jsonPath("$.face").exists())
      .andExpect(jsonPath("$.suit").exists());
  }

  @Test
  public void dealOneCardNonExistentDeckTest()
    throws Exception {

    doThrow(new IllegalArgumentException("Invalid DeckId")).when(service).dealCard("1");

    mvc.perform(get("/decks/1/card")
      .contentType(MediaType.APPLICATION_JSON))
      .andExpect(status().isNotFound());
  }

  @Test
  public void dealOneCardEndOfDeckTest()
    throws Exception {

    Deck deck = new PlayingCardDeck();

    given(service.dealCard(deck.getId())).willReturn(null);

    mvc.perform(get("/decks/" + deck.getId() + "/card")
      .contentType(MediaType.APPLICATION_JSON))
      .andExpect(status().isOk())
      .andExpect(jsonPath("$.face").doesNotExist())
      .andExpect(jsonPath("$.suit").doesNotExist());
  }


}
