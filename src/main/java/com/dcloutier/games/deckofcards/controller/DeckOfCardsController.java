package com.dcloutier.games.deckofcards.controller;

import com.dcloutier.games.deckofcards.model.Deck;
import com.dcloutier.games.deckofcards.model.PlayingCard;
import com.dcloutier.games.deckofcards.service.DeckOfCardsService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

/**
 * Controller class that handles requests
 */
@RestController()
@RequestMapping("/decks")
@Api(value = "deckofcards", description = "Operations on a deck of playing cards")
public class DeckOfCardsController {

  @Autowired
  private DeckOfCardsService deckOfCardsService;

  /**
   * Creates a new deck of cards.
   *
   * @return the deck of cards just created
   */
  @PostMapping
  public ResponseEntity<Deck> createDeck() {

    Deck deck = deckOfCardsService.createDeck();
    URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(deck.getId()).toUri();

    return ResponseEntity.created(location).body(deck);
  }

  /**
   * Retreives an existing deck of cards.
   *
   * @param id  id of the deck to retreive
   * @return        the deck of card
   */
  @GetMapping("/{id}")
  public ResponseEntity<Deck> retreiveDeck(@PathVariable String id) {

    Deck deck = null;
    deck = deckOfCardsService.retreiveDeck(id);

    return deck != null ? ResponseEntity.ok().body(deck) : ResponseEntity.notFound().build();
  }

  /**
   * Shuffle the remaining cards of a deck.
   *
   * @param id  Id of the deck to shuffle
   */
  @PostMapping("/{id}/shuffled")
  public ResponseEntity<?> shuffleDeck(@PathVariable String id) {

    try {
      deckOfCardsService.shuffle(id);
    } catch (IllegalArgumentException e) {
      return ResponseEntity.notFound().build();
    }

    return ResponseEntity.noContent().build();
  }

  /**
   * Deal a card from a deck with the corresponding Id
   *
   * @param id  Id of the deck
   * @return        A card from the deck
   */
  @GetMapping("/{id}/card")
  public ResponseEntity<PlayingCard> dealCard(@PathVariable String id) {

    PlayingCard card = null;
    try {
      card = deckOfCardsService.dealCard(id);
    } catch (IllegalArgumentException e) {
      return ResponseEntity.notFound().build();
    }
    return ResponseEntity.ok().body(card);
  }
}
