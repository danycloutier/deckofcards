package com.dcloutier.games.deckofcards.model;

/**
 * Class reprensenting one of the fifty-two cards of a standard deck of playing cards.
 * It has a suit and a value.
 */
public class PlayingCard {

  protected Faces face;
  protected Suits suit;

  /**
   * Construct a card with the specified suit and value
   *
   * @param face the face value of this card
   * @param suit the suit value of this card
   */
  public PlayingCard(Faces face, Suits suit) {
    if (face == null)
      throw new IllegalArgumentException("face cannot be null");

    if (suit == null)
      throw new IllegalArgumentException("face cannot be null");

    this.face = face;
    this.suit = suit;
  }

  /**
   * Return the face value
   *
   * @return the face value
   */
  public Faces getFace() {
    return face;
  }

  /**
   * Return the suit value
   *
   * @return the suit value
   */
  public Suits getSuit() {
    return suit;
  }
}
