package com.dcloutier.games.deckofcards.model;

/**
 * Interface containing operations to execute on a deck of cards.
 */
public interface Deck {

  /**
   * Shuffle the remaining cards of the deck
   */
  void shuffle();

  /**
   * Return the shuffled indicator.
   *
   * @return a boolean that indicates if the deck is shuffled
   */
  boolean getShuffled();

  /**
   * Return the remaining number of cards in the deck
   *
   * @return the remaining number of cards in the deck
   */
  int getRemaining();

  /**
   * Returns the id of the deck
   *
   * @return the id of the deck
   */
  String getId();

  /**
   * Deal one card of the deck. If there is no cards left in the deck, null is returned
   *
   * @return one card of the deck or null if there is no card left in the deck
   */
  PlayingCard dealOneCard();
}
