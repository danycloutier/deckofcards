package com.dcloutier.games.deckofcards.model;

/**
 * Enums of differents suits in a playing card deck
 */
public enum Suits {
  HEARTS,
  SPADES,
  CLUBS,
  DIAMONDS
}
