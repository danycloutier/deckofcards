package com.dcloutier.games.deckofcards.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

/**
 * Class representing a deck of poker-style playing cards. (Fifty-two playing cards in four
 * suits: hearts, spades, clubs, diamonds, with face values of Ace, 2-10, Jack, Queen, and King.)
 */
public class PlayingCardDeck implements Deck {

  private List<PlayingCard> cards;
  private boolean shuffled = false;
  private String id;

  /**
   * Construct a Fifty-two playing cards in four suits: hearts, spades, clubs, diamonds,
   * with face values of Ace, 2-10, Jack, Queen, and King. The deck is created in sorted order.
   */
  public PlayingCardDeck() {
    initDeck();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void shuffle() {

    //Reference : https://en.wikipedia.org/wiki/Fisher–Yates_shuffle
    PlayingCard temp;
    int randomIndice = 0;
    int size = cards.size();
    Random random = new Random();

    for (int i = 0; i < size; i++) {
      randomIndice = i + random.nextInt(size - i);
      temp = cards.get(randomIndice);
      cards.set(randomIndice, cards.get(i));
      cards.set(i, temp);

    }
    shuffled = true;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean getShuffled() {
    return shuffled;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public int getRemaining() {
    return cards.size();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getId() {
    return id;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public PlayingCard dealOneCard() {

    if (cards.isEmpty())
      return null;

    return cards.remove(0);
  }

  private void initDeck() {
    id = UUID.randomUUID().toString();
    cards = new ArrayList<>();

    for (Suits suit : Suits.values()) {
      for (Faces face : Faces.values()) {
        cards.add(new PlayingCard(face, suit));
      }
    }
  }
}
