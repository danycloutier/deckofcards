package com.dcloutier.games.deckofcards.model;

/**
 * Enums of differents faces in a playing card deck
 */
public enum Faces {
  ACE,
  TWO,
  THREE,
  FOUR,
  FIVE,
  SIX,
  SEVEN,
  EIGHT,
  NINE,
  TEN,
  JACK,
  QUEEN,
  KING
}
