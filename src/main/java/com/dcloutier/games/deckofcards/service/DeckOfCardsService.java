package com.dcloutier.games.deckofcards.service;

import com.dcloutier.games.deckofcards.model.Deck;
import com.dcloutier.games.deckofcards.model.PlayingCard;
import com.dcloutier.games.deckofcards.model.PlayingCardDeck;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * A service that provides operations around managing decks of cards.
 */
@Component
public class DeckOfCardsService {

  private static Map<String, Deck> decks = new ConcurrentHashMap<>();

  /**
   * Creates a new deck of cards.
   *
   * @return the deck of cards just created
   */
  public Deck createDeck() {
    Deck deck;
    deck = new PlayingCardDeck();
    decks.put(deck.getId(), deck);

    return deck;
  }

  /**
   * Retreives an existing deck of cards.
   *
   * @param deckId  id of the deck to retreive
   * @return        the deck of card
   */
  public Deck retreiveDeck(String deckId) {
    if (deckId == null)
      throw new IllegalArgumentException("deckId cannot be null");

    Deck deck = decks.get(deckId);
    return deck;
  }

  /**
   * Shuffle the remaining cards of a deck.
   *
   * @param deckId  Id of the deck to shuffle
   */
  public void shuffle(String deckId) {
    Deck deck = retreiveDeck(deckId);

    if (deck == null)
      throw new IllegalArgumentException("Deck not found");

    deck.shuffle();
  }

  /**
   * Deal a card from a deck with the corresponding Id
   *
   * @param deckId  Id of the deck
   * @return        A card from the deck
   */
  public PlayingCard dealCard(String deckId) {
    Deck deck = retreiveDeck(deckId);

    if (deck == null)
      throw new IllegalArgumentException("Deck not found");

    return deck.dealOneCard();
  }
}
