**To run the application :**

mvn clean install

java -jar deckofcards-0.0.1.jar


**Urls :**

Create a deck :
POST http://localhost:8080/decks

Shuffle a deck :
POST http://localhost:8080/decks/{id}/shuffled

Deal one card from a deck :
GET http://localhost:8080/decks/{id}/card

Get a deck :
GET http://localhost:8080/decks/{id}

Swagger-ui :
http://localhost:8080/swagger-ui.html